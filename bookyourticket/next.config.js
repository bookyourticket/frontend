/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  images: {
    domains: ["m.media-amazon.com", "assets-in.bmscdn.com"],
  },
};

module.exports = nextConfig;
