interface IChipProps {
  text: string;
}

const Chip = (props: IChipProps) => {
  return (
    <div className="inline-block py-0.5 px-2 text-[10px] text-white/50 rounded-[11px] border-white/50 border">
      {props.text.toUpperCase()}
    </div>
  );
};

export default Chip;
