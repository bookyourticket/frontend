import Image from "next/image";

interface IProfileProps {
  profile: TProfile;
  imageStyle?: {
    width?: number;
    height?: number;
  };
}

export type TProfile = {
  image: string;
  heading: string;
  subHeading: string;
  id: string;
};

const Profile = ({
  profile: { image, heading, subHeading, id },
  imageStyle: { width = 120, height = 120 } = {},
}: IProfileProps) => {
  return (
    <div className={`inline-block text-[#000] max-w-[${width}px]`}>
      <Image
        className="rounded-full"
        src={image}
        alt={heading}
        width={width}
        height={height}
      />
      <h5 className="pt-2 text-center">{heading}</h5>
      <h6 className="text-center text-sm text-[#666]">{subHeading}</h6>
    </div>
  );
};

export default Profile;
