interface IHeading {
  text: string;
  className?: string;
}

const Heading = (props: IHeading) => {
  const { text, className = "text-[#000]" } = props;

  return (
    <h3 className={`text-2xl font-bold leading-7 ${className}`}>{text}</h3>
  );
};

export default Heading;
