import MagnifyingGlass from "../SvgComponents/MagnifyingGlass";

const Search = () => {
  return (
    <div className="inline-block rounded overflow-hidden h-full w-full">
      <div className="flex w-full">
        <div className="bg-white px-3 py-[10px] flex items-center">
          <MagnifyingGlass width="13px" height="13px" />
        </div>
        <input
          type="text"
          placeholder="Search for Movies, Events, Plays, Sports and Activities"
          className="w-full max-w-[500px] text-[14px] py-2 text-black focus:outline-none"
        />
      </div>
    </div>
  );
};

export default Search;
