import { CSSProperties } from "react";

interface IRatings {
  score: number;
  total: number;
  className?: string;
}

const Ratings = ({ score, total, className }: IRatings) => {
  return (
    <>
      <span className={className}>{score}</span>
      <span className={className}>/</span>
      <span className={className}>{total}</span>
    </>
  );
};

export default Ratings;
