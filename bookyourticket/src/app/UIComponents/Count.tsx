const Count = ({ number }: { number: number }) => {
  return <span>{number}</span>;
};

export default Count;
