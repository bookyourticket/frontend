// The combination of Starred, score out of 10, total votes is used everywhere in the app
// hence clubbing them together in one component

import Count from "./Count";
import Ratings from "./Ratings";
import Stars from "./Stars";

interface IStarRatingVotes {
  starClasses?: string;
  ratingsClasses?: string;
  countClasses?: string;
  containerClasses?: string;
  countContainerClasses?: string;
  ratings: number;
  votes: number;
}

const StarRatingVotes = (props: IStarRatingVotes) => {
  const {
    starClasses,
    ratingsClasses,
    countClasses,
    containerClasses,
    countContainerClasses,
    votes,
  } = props;

  return (
    <div className={`text-white flex items-center ${containerClasses}`}>
      <div className="mr-2 flex items-center">
        <Stars filledCount={0} count={1} className={starClasses} />
      </div>
      <div className="text-[#F5F5F5]">
        <Ratings score={7.7} total={10} className={ratingsClasses} />
      </div>
      <div className={`pl-2 ${countContainerClasses}`}>
        <Count number={votes} /> votes
      </div>
    </div>
  );
};

export default StarRatingVotes;
