import Star from "./Star";

const Stars = ({
  count,
  filledCount,
  className,
}: {
  count: number;
  filledCount: number;
  className?: string;
}) => {
  return (
    <>
      {[...Array(count)].map((_, index) => (
        <Star
          key={index}
          isFilled={index < filledCount}
          className={className}
        />
      ))}
    </>
  );
};

export default Stars;
