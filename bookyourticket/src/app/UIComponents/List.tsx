export type TElement = { id: string; text: string };
export type TAlignment = "horizontal" | "vertical";

interface IListProps {
  elements: TElement[];
  alignment: TAlignment;
}

const List = ({ elements, alignment = "vertical" }: IListProps) => {
  return alignment === "horizontal" ? (
    <ul className="flex list-disc list-inside tracking-[0.2px]">
      {elements.map(({ id, text }) => (
        <li key={id} className="mr-3">
          {text}
        </li>
      ))}
    </ul>
  ) : (
    <ul className="list-disc">
      {elements.map(({ id, text }) => (
        <li key={id}>{text}</li>
      ))}
    </ul>
  );
};

export default List;
