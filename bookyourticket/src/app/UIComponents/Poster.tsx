import React from "react";
import Image from "next/image";

const Poster = ({
  image,
  width = 222,
  height = 348,
}: {
  image: string;
  width?: number | `${number}` | undefined;
  height?: number | `${number}` | undefined;
}) => {
  return (
    <div className="relative" style={{ height, width }}>
      <Image
        className="Image"
        src={image}
        alt="Landscape photograph by Tobias Tullius"
        fill
        priority
      />
    </div>
  );
};

export default Poster;
