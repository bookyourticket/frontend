interface ICertificateProps {
  certificate: string;
}

const Certificate = (props: ICertificateProps) => {
  return (
    <div className="border-2 rounded-full w-[25px] h-[25px] flex items-center justify-center text-xs">
      <div className="leading-5">{props.certificate}</div>
    </div>
  );
};

export default Certificate;
