import Container from "@/app/LayoutComponents/Container";
import Heading from "@/app/UIComponents/Heading";

interface IAbout {
  about: string;
}

const About = (props: IAbout) => {
  const { about } = props;

  return (
    <section className="py-8 border-b border-neutral-200">
      <Container>
        <Heading text="About the movie" />
        <div className="mt-4">
          <p className="text-[#333]">{about}</p>
        </div>
      </Container>
    </section>
  );
};

export default About;
