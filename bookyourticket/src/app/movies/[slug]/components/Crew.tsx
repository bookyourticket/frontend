import Container from "@/app/LayoutComponents/Container";
import Flex from "@/app/LayoutComponents/Flex";
import Heading from "@/app/UIComponents/Heading";
import Profile, { TProfile } from "@/app/UIComponents/Profile";
import { TMovieCrew } from "@/app/types/movieDetails";

interface ICrewProps {
  crew: TMovieCrew[];
}

const Crew = (props: ICrewProps) => {
  const { crew } = props;

  const profileDataFormatted: TProfile[] = crew.map(
    ({ id, name, movieRole, image }) => ({
      id,
      image,
      heading: name,
      subHeading: movieRole,
    })
  );

  return (
    <section className="py-8 border-b border-neutral-200">
      <Container>
        <Heading text="Crew" />
        <Flex flexContainerClasses="mt-4 items-start">
          {profileDataFormatted.map((castMember) => (
            <div key={castMember.id} className="mr-8">
              <Profile profile={castMember} />
            </div>
          ))}
        </Flex>
      </Container>
    </section>
  );
};

export default Crew;
