import Flex from "@/app/LayoutComponents/Flex";
import List, { TElement } from "@/app/UIComponents/List";
import Poster from "@/app/UIComponents/Poster";
import StarRatingVotes from "@/app/UIComponents/StarRatingVotes";
import concatenated from "@/app/Utils/concatenated";
import Link from "next/link";
import { TMovieDetails } from "../../../types/movieDetails";
import dayjs from "dayjs";
import { getHoursAndMinutesFromMinutes } from "@/app/Utils/duration";

type IShowcaseProps = {
  movieDetails: TMovieDetails;
};

const Showcase = (props: IShowcaseProps) => {
  const { movieDetails } = props;

  const [durationHours, durationMinutes] = getHoursAndMinutesFromMinutes(
    movieDetails.duration
  );

  const fieldsToConcatenate: TElement[] = [
    {
      id: "duration",
      text: `${durationHours}h ${durationMinutes}m`,
    },
    {
      id: "genres",
      text: concatenated(movieDetails.genres.map((genre) => genre.text)),
    },
    {
      id: "certificate",
      text: movieDetails.certificate,
    },
    {
      id: "releaseDate",
      text: dayjs(movieDetails.releaseDate).format("D MMM, YYYY"),
    },
  ];

  return (
    <section>
      <div
        style={{
          backgroundImage: `linear-gradient(90deg, rgb(26, 26, 26) 24.97%, rgb(26, 26, 26) 38.3%, rgba(26, 26, 26, 0.04) 97.47%, rgb(26, 26, 26) 100%), url(${movieDetails.posterUrl})`,
        }}
        className="p-8 bg-cover"
      >
        <Flex flexContainerClasses="items-center">
          <Poster image={movieDetails.posterUrl} width={260} height={392} />

          <div className="pl-8 flex-grow max-w-[520px]">
            <h1 className="text-4xl font-bold">{movieDetails.title}</h1>
            <div className="mt-6">
              <StarRatingVotes
                starClasses="w-8"
                ratingsClasses="text-2xl font-bold"
                countContainerClasses="mr-2 pt-1"
                ratings={movieDetails.ratings}
                votes={movieDetails.votes}
              />
            </div>
            <div className="mt-4 bg-[#333333] py-3 px-6 rounded-lg max-w-[446px]">
              <Flex flexContainerClasses="justify-between items-center">
                <div>
                  <h5 className="font-medium text-[18px] tracking-wide leading-6">
                    Add your rating & review
                  </h5>
                  <h6 className="pt-0.5 text-[#ccc]">Your ratings matter</h6>
                </div>
                <div className="flex items-center py-2 px-3 text-[#333] bg-white rounded-lg outline-none text-[18px] font-medium max-w-[446px]">
                  <button>Rate now</button>
                </div>
              </Flex>
            </div>
            <div className="inline-block mt-6">
              <button className="py-0.5 px-2 text-black bg-[#e5e5e5] rounded-sm">
                {movieDetails.languages.reduce((acc, curr) => {
                  if (acc.length === 0) {
                    return curr;
                  }
                  return `${acc}, ${curr}`;
                }, "")}
              </button>
            </div>
            <div className="mt-4">
              <List elements={fieldsToConcatenate} alignment="horizontal" />
            </div>
            <div className="max-w-[216px] mt-8">
              <Link href={`/tickets/movies/${movieDetails.slug}`}>
                <div className="py-3 px-2 bg-[#f84464] rounded-lg text-center">
                  Book Tickets
                </div>
              </Link>
            </div>
          </div>
        </Flex>
      </div>
    </section>
  );
};

export default Showcase;
