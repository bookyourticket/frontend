import Container from "@/app/LayoutComponents/Container";
import Flex from "@/app/LayoutComponents/Flex";
import Heading from "@/app/UIComponents/Heading";
import Profile, { TProfile } from "@/app/UIComponents/Profile";
import { TMovieCast } from "@/app/types/movieDetails";

interface ICastProps {
  cast: TMovieCast[];
}

const Cast = (props: ICastProps) => {
  const { cast } = props;

  const profileDataFormatted: TProfile[] = cast.map(
    ({ id, name, movieCharacterName, image }) => ({
      id,
      image,
      heading: name,
      subHeading: `as ${movieCharacterName}`,
    })
  );

  return (
    <section className="py-8 border-b border-neutral-200">
      <Container>
        <Heading text="Cast" />
        <Flex flexContainerClasses="mt-4 items-start">
          {profileDataFormatted.map((castMember) => (
            <div key={castMember.id} className="mr-8">
              <Profile profile={castMember} />
            </div>
          ))}
        </Flex>
      </Container>
    </section>
  );
};

export default Cast;
