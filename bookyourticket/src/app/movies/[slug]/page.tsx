import Navbar from "@/app/components/Navbar";
import Showcase from "./components/Showcase";
import About from "./components/About";
import Cast from "./components/Cast";
import Crew from "./components/Crew";
import { getMovieById } from "@/app/api/movies/movies";
import { TMovieDetails } from "@/app/types/movieDetails";
import { formatMovieDetails } from "@/app/Utils/formatMovieDetails";

const Movie = async () => {
  const movieDetails: TMovieDetails = formatMovieDetails(
    await getMovieById("1234")
  );

  return (
    <div>
      <Navbar />
      <Showcase movieDetails={movieDetails} />
      <About about={movieDetails.about} />
      <Cast cast={movieDetails.cast} />
      <Crew crew={movieDetails.crew} />
    </div>
  );
};

export default Movie;
