import RecommendedMovies from "./components/RecommendedMovies";
import Navbar from "./components/Navbar";
import { getMovies } from "./api/movies/movies";

export default async function Home() {
  const movies = await getMovies();

  return (
    <>
      <Navbar />
      <div className="pb-[32px] h-0">&nbsp;</div>
      <RecommendedMovies movies={movies} />
    </>
  );
}
