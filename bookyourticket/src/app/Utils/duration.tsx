import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
dayjs.extend(duration);

export const getHoursAndMinutesFromMinutes = (
  minutes: number
): [number, number] => {
  const duration = dayjs.duration(minutes, "minutes");
  const hours = duration.hours();
  const remainingMinutes = duration.minutes();

  return [hours, remainingMinutes];
};
