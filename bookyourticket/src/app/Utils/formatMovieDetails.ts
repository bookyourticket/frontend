import dayjs from "dayjs";
import { TMovieDetails, TServerMovieDetails } from "../types/movieDetails";

export const formatMovieDetails = (
  dataInServerFormat: TServerMovieDetails
): TMovieDetails => {
  return {
    ...dataInServerFormat,
    releaseDate: dayjs(dataInServerFormat.releaseDate).toDate(),
  };
};
