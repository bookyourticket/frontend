const concatenated = (genres: string[], seperator: string = "/"): string => {
  return genres.reduce((acc, genre, index) => {
    acc += genre;
    if (index === genres.length - 1) {
      return acc;
    }

    return acc + seperator;
  }, "");
};

export default concatenated;
