export type TTheatre = {
  id: string;
  name: string;
  shows: {
    id: string;
    time: string;
  }[];
};
