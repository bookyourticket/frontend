export type TMovieDetails = {
  id: string;
  title: string;
  slug: string;
  ratings: number;
  votes: number;
  posterUrl: string;
  duration: number; // in minutes
  genres: IMovieFieldsWithIDAndText[];
  certificate: string;
  releaseDate: Date;
  languages: string[];
  showcaseUrl: string;
  about: string;
  cast: TMovieCast[];
  crew: TMovieCrew[];
};

export type IMovieFieldsWithIDAndText = {
  text: string;
  id: string;
};

export type TServerMovieDetails = Omit<TMovieDetails, "releaseDate"> & {
  releaseDate: string;
};

export type TMovieCast = {
  id: string;
  name: string;
  movieCharacterName: string;
  image: string;
};

export type TMovieCrew = {
  id: string;
  name: string;
  movieRole: string;
  image: string;
};
