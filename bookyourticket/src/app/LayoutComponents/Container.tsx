type ContainerProps = { className?: string; children: React.ReactNode };

const Container = (props: ContainerProps) => {
  return (
    <div className={`max-w-7xl m-auto w-[92%] ${props.className}`}>
      {props.children}
    </div>
  );
};

export default Container;
