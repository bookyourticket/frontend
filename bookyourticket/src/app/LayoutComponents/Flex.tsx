const Flex = ({
  children,
  flexContainerClasses = "",
}: {
  children: React.ReactNode;
  flexContainerClasses?: string;
}) => {
  return <div className={`flex ${flexContainerClasses}`}>{children}</div>;
};

export default Flex;
