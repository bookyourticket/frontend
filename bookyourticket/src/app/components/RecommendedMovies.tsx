import Heading from "../UIComponents/Heading";
import { IMoviePoster } from "./MoviePoster";
import MoviesList from "./MoviesList";

interface IRecommendedMoviesProps {
  movies: IMoviePoster[];
}

const RecommendedMovies = (props: IRecommendedMoviesProps) => {
  const { movies } = props;

  return (
    <div className="max-w-7xl m-auto w-[92vw]">
      <Heading text="Recommended Movies" className="text-[#333] mb-2" />
      <MoviesList movies={movies} />
    </div>
  );
};

export default RecommendedMovies;
