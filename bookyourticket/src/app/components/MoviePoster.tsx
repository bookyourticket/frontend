import Poster from "../UIComponents/Poster";
import StarRatingVotes from "../UIComponents/StarRatingVotes";
import concatenated from "../Utils/concatenated";
import { TMovieDetails } from "../types/movieDetails";

export type IMoviePoster = Pick<
  TMovieDetails,
  "title" | "id" | "genres" | "posterUrl" | "slug" | "ratings" | "votes"
>;

const MoviePoster = (props: IMoviePoster) => {
  const { id, genres, posterUrl, title, ratings, votes } = props;

  return (
    <div className="max-w-[222px] inline-block">
      <div className="rounded-lg overflow-hidden">
        <Poster image={posterUrl} />
        <StarRatingVotes
          ratings={ratings}
          starClasses="w-5"
          containerClasses="p-1 bg-black"
          votes={votes}
        />
      </div>
      <div className="mt-1">
        <h3 className="text-[#222] mb-1 text-[18px] font-medium">{title}</h3>
        <h4 className="text-[#666] text-[16px] leading-6">
          {concatenated(genres.map((genre) => genre.text))}
        </h4>
      </div>
    </div>
  );
};

export default MoviePoster;
