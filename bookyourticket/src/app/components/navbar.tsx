import Container from "../LayoutComponents/Container";
import BookYourTicket from "../SvgComponents/BookYourTicket";
import Search from "../UIComponents/Search";

const Navbar = () => {
  return (
    <nav className="bg-[#333545] h-[64px]">
      <Container className="h-full">
        <div className="flex flex-row-reverse h-full w-full">
          <div className="h-full flex items-center">
            <button className="w-[70px] h-6 bg-[#f84464] text-xs rounded border border-[#f84464] border-solid">
              Sign in
            </button>
          </div>
          <div className="h-full grow">
            <div className="flex items-center h-full w-3/4">
              <div>
                <BookYourTicket />
              </div>
              <div className="ml-4 w-[calc(100%-10rem)] flex items-center">
                <Search />
              </div>
            </div>
          </div>
        </div>
      </Container>
    </nav>
  );
};

export default Navbar;
