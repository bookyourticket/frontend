import Link from "next/link";
import MoviePoster, { IMoviePoster } from "./MoviePoster";

const MoviesList = (props: { movies: IMoviePoster[] }) => {
  return (
    <div className="grid grid-cols-5 gap-8">
      {props.movies.map((movie) => (
        <Link key={movie.id} href={`/movies/${movie.slug}`}>
          <MoviePoster key={movie.id} {...movie} />
        </Link>
      ))}
    </div>
  );
};

export default MoviesList;
