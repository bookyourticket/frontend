import { TTheatre } from "@/app/types/theatre";

export const getTheatres = (): Promise<TTheatre[]> => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res([
        {
          id: "123",
          name: "Cinepolis: Seasons Mall, Pune",
          shows: [
            {
              id: "1231",
              time: "11:40 PM",
            },
          ],
        },
        {
          id: "124",
          name: "Cinepolis: Nexus WESTEND Mall Aundh, Pune",
          shows: [
            {
              id: "1232",
              time: "11:35 PM",
            },
          ],
        },
        {
          id: "125",
          name: "PVR: City One Mall, Pimpri",
          shows: [
            {
              id: "1233",
              time: "08:30 PM",
            },
            {
              id: "1234",
              time: "11:30 PM",
            },
          ],
        },
      ]);
    }, 2000);
  });
};
