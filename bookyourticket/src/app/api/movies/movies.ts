import { TMovieDetails, TServerMovieDetails } from "../../types/movieDetails";

export const getMovies = (): Promise<TServerMovieDetails[]> => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res([
        {
          title: "Evil Dead Rise",
          id: "1234",
          slug: "evil-dead-rise-1234",
          ratings: 7.7,
          votes: 2200,
          showcaseUrl:
            "https://assets-in.bmscdn.com/iedb/movies/images/mobile/listing/xxlarge/evil-dead-rise-et00349309-1681275399.jpg",
          posterUrl:
            "https://assets-in.bmscdn.com/iedb/movies/images/mobile/thumbnail/xlarge/evil-dead-rise-et00349309-1681275399.jpg",
          duration: 99,
          genres: [
            {
              id: "12",
              text: "Fantasy",
            },
            {
              id: "13",
              text: "Horror",
            },
            {
              id: "14",
              text: "Thriller",
            },
          ],
          certificate: "A",
          releaseDate: "2023-04-21T14:48:00.000Z",
          languages: ["English"],
          cast: [
            {
              id: "1",
              name: "Alyssa Sutherland",
              movieCharacterName: "Ellie",
              image:
                "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
            },
            {
              id: "2",
              name: "Lily sullivan",
              movieCharacterName: "Beth",
              image:
                "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/lily-sullivan-2025063-1672897952.jpg",
            },
            {
              id: "3",
              name: "Morgan Davies",
              movieCharacterName: "Danny",
              image:
                "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
            },
            {
              id: "4",
              name: "Nell Fisher",
              movieCharacterName: "Kassie",
              image:
                "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
            },
            {
              id: "5",
              name: "Jayden Daniels",
              movieCharacterName: "Gabriel",
              image:
                "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
            },
            {
              id: "6",
              name: "Gabrielle Echols",
              movieCharacterName: "Bridget",
              image:
                "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
            },
          ],
          crew: [
            {
              id: "1",
              name: "Lee Cronin",
              movieRole: "Director, Writer",
              image:
                "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/lee-cronin-2011119-1672899148.jpg",
            },
          ],
          about:
            'Moving the action out of the woods and into the city, "Evil Dead Rise" tells a twisted tale of two estranged sisters, played by Sutherland and Sullivan, whose reunion is cut short by the rise of flesh-possessing demons, thrusting them into a primal battle for survival as they face the most nightmarish version of family imaginable.',
        },
        {
          title: "Bholaa",
          id: "1235",
          slug: "bholaa-1235",
          ratings: 7.2,
          votes: 1100,
          showcaseUrl:
            "https://assets-in.bmscdn.com/iedb/movies/images/mobile/listing/xxlarge/evil-dead-rise-et00349309-1681275399.jpg",
          posterUrl:
            "https://m.media-amazon.com/images/M/MV5BNjc1Y2U2ZGUtMGMwOS00NTVhLWE0MTUtMDI1YmFiZTdmY2E2XkEyXkFqcGdeQXVyMTA3MDk2NDg2._V1_FMjpg_UX999_.jpg",
          duration: 117,
          genres: [
            {
              id: "9",
              text: "Drama",
            },
            {
              id: "10",
              text: "Action",
            },
            {
              id: "14",
              text: "Thriller",
            },
          ],
          certificate: "A",
          releaseDate: "2023-04-21T14:48:00.000Z",
          languages: ["Hindi"],
          cast: [],
          crew: [],
          about:
            "Bholaa, a prisoner, is finally going home after 10 years of imprisonment to meet his young daughter. His journey gets difficult when he is arrested mid-way. At first, he is not aware of the grave situation he has got himself into but after a crazy incident takes place, he must travel a pathway full of crazy obstacles with death lurking around every corner. Will he get to meet his daughter?",
        },
      ]);
    }, 2000);
  });
};

export const getMovieById = (id: string): Promise<TServerMovieDetails> => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res({
        title: "Evil Dead Rise",
        id: "1234",
        slug: "evil-dead-rise-1234",
        ratings: 7.7,
        votes: 2200,
        posterUrl:
          "https://assets-in.bmscdn.com/iedb/movies/images/mobile/thumbnail/xlarge/evil-dead-rise-et00349309-1681275399.jpg",
        showcaseUrl:
          "https://assets-in.bmscdn.com/iedb/movies/images/mobile/listing/xxlarge/evil-dead-rise-et00349309-1681275399.jpg",
        duration: 99,
        genres: [
          {
            id: "12",
            text: "Fantasy",
          },
          {
            id: "13",
            text: "Horror",
          },
          {
            id: "14",
            text: "Thriller",
          },
        ],
        certificate: "A",
        releaseDate: "2023-04-21T14:48:00.000Z",
        languages: ["English"],
        cast: [
          {
            id: "1",
            name: "Alyssa Sutherland",
            movieCharacterName: "Ellie",
            image:
              "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
          },
          {
            id: "2",
            name: "Lily sullivan",
            movieCharacterName: "Beth",
            image:
              "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/lily-sullivan-2025063-1672897952.jpg",
          },
          {
            id: "3",
            name: "Morgan Davies",
            movieCharacterName: "Danny",
            image:
              "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
          },
          {
            id: "4",
            name: "Nell Fisher",
            movieCharacterName: "Kassie",
            image:
              "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
          },
          {
            id: "5",
            name: "Jayden Daniels",
            movieCharacterName: "Gabriel",
            image:
              "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
          },
          {
            id: "6",
            name: "Gabrielle Echols",
            movieCharacterName: "Bridget",
            image:
              "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/alyssa-sutherland-2025062-1672897844.jpg",
          },
        ],
        crew: [
          {
            id: "1",
            name: "Lee Cronin",
            movieRole: "Director, Writer",
            image:
              "https://assets-in.bmscdn.com/iedb/artist/images/website/poster/large/lee-cronin-2011119-1672899148.jpg",
          },
        ],
        about:
          'Moving the action out of the woods and into the city, "Evil Dead Rise" tells a twisted tale of two estranged sisters, played by Sutherland and Sullivan, whose reunion is cut short by the rise of flesh-possessing demons, thrusting them into a primal battle for survival as they face the most nightmarish version of family imaginable.',
      });
    }, 2000);
  });
};
