import { TTheatre } from "@/app/types/theatre";
import Theatre from "./Theatre";

interface ITheatresProps {
  theatres: TTheatre[];
}

const Theatres = (props: ITheatresProps) => {
  const { theatres } = props;

  return (
    <section className="bg-white min-h-[600px]">
      {theatres.map((theatre) => (
        <Theatre key={theatre.id} theatre={theatre} />
      ))}
    </section>
  );
};

export default Theatres;
