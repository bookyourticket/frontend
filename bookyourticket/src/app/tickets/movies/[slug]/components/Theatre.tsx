import Phone from "@/app/SvgComponents/Phone";
import Food from "@/app/SvgComponents/Food";
import Flex from "@/app/LayoutComponents/Flex";
import Link from "next/link";
import { TTheatre } from "@/app/types/theatre";

const Theatre = ({ theatre }: { theatre: TTheatre }) => {
  return (
    <Flex flexContainerClasses="w-full border border-b-[#eff1f3] py-[13px] px-[20px]">
      <div className="min-w-[400px]">
        <h4 className="text-[#333] font-bold text-sm">{theatre.name}</h4>
        <div className="mt-[10px]">
          <Flex>
            <Flex flexContainerClasses="ml-[-3px] items-center">
              <Phone />
              <span className="text-[#49ba8e] ml-2 text-xs">M-Ticket</span>
            </Flex>
            <Flex flexContainerClasses="ml-6 items-center">
              <Food />
              <span className="text-[#ffa426] ml-2 text-xs">
                Food & Beverage
              </span>
            </Flex>
          </Flex>
        </div>
      </div>
      <div>
        <Flex>
          {theatre.shows.map((show) => (
            <div
              key={show.id}
              className="border border-[#999] w-[110px] p-1 rounded h-10 m-2 hover:cursor-pointer"
            >
              <Flex flexContainerClasses="items-center justify-center h-full">
                <Link href={`/tickets/movies/bholaa/seats`}>
                  <span className="text-[#4abd5d] text-xs">{show.time}</span>
                </Link>
              </Flex>
            </div>
          ))}
        </Flex>
      </div>
    </Flex>
  );
};

export default Theatre;
