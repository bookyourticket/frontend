import dayjs from "dayjs";

interface IEventDateProps {
  date: Date;
  selected: boolean;
}

const EventDate = (props: IEventDateProps) => {
  const { date, selected } = props;
  const dateFn = dayjs(date);

  const wrapperClasses = selected ? "#f84464" : "#fff";
  const textClasses = selected ? "#fff" : "#333";
  const subTextClasses = selected ? "#fff" : "#666";

  const hoverClasses = selected ? "" : "#666";

  return (
    <div
      className={`bg-[${wrapperClasses}] rounded-lg p-[5px] flex flex-col w-[50px] justify-center text-center hover:cursor-pointer`}
    >
      <div
        className={`text-[10px] font-medium leading-3 pb-[1px] text-[${subTextClasses}]`}
      >
        {dateFn.format("ddd").toUpperCase()}
      </div>
      <div
        className={`font-medium leading-[18px] text-[${textClasses}] hover:text-[${hoverClasses}]`}
      >
        {dateFn.date()}
      </div>
      <div className={`text-[10px] font-medium text-[${subTextClasses}]`}>
        {dateFn.format("MMM").toUpperCase()}
      </div>
    </div>
  );
};

export default EventDate;
