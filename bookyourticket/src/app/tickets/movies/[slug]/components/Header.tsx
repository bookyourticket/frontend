import Container from "@/app/LayoutComponents/Container";
import Flex from "@/app/LayoutComponents/Flex";
import Certificate from "@/app/UIComponents/Certificate";
import Chip from "@/app/UIComponents/Chip";
import EventDate from "./EventDate";
import dayjs from "dayjs";
import ChevronLeft from "@/app/SvgComponents/ChevronLeft";
import ChevronRight from "@/app/SvgComponents/ChevronRight";
import { TMovieDetails } from "@/app/types/movieDetails";

const NEXT_N_DAYS = 5;

const getNDays = () => {
  return [...Array(NEXT_N_DAYS)].map((_, index) =>
    dayjs(new Date()).add(index, "day").toDate()
  );
};

interface IHeaderProps {
  movie: TMovieDetails;
}

const Header = (props: IHeaderProps) => {
  const { movie } = props;
  const days = getNDays();

  return (
    <>
      <section className="bg-[#333545]">
        <Container>
          <div className="py-[16px]">
            <h2 className="text-4xl">{movie.title}</h2>
            <Flex flexContainerClasses="mt-2">
              <div className="mr-4 inline-block">
                <Certificate certificate={movie.certificate} />
              </div>
              {movie.genres.map((genre) => (
                <div key={genre.id} className="inline-block mr-[5px]">
                  <Chip text={genre.text} />
                </div>
              ))}
            </Flex>
          </div>
        </Container>
      </section>
      <section>
        <Container>
          <div className="my-[6px]">
            <Flex flexContainerClasses="items-center">
              <ChevronLeft />
              {days.map((day, index) => (
                <EventDate key={index} date={day} selected={index === 0} />
              ))}
              <ChevronRight />
            </Flex>
          </div>
        </Container>
      </section>
    </>
  );
};

export default Header;
