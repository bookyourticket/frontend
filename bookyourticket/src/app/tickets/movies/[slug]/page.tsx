import Navbar from "@/app/components/Navbar";
import Header from "./components/Header";
import Theatres from "./components/Theatres";
import { getMovieById } from "@/app/api/movies/movies";
import { formatMovieDetails } from "@/app/Utils/formatMovieDetails";
import { getTheatres } from "@/app/api/venues/theatres";

const MovieTicketBooking = async () => {
  const movie = formatMovieDetails(await getMovieById("1234"));
  const theatres = await getTheatres();

  return (
    <>
      <Navbar />
      <Header movie={movie} />
      <div className="bg-[#f2f2f2] p-[15px]">
        <Theatres theatres={theatres} />
      </div>
    </>
  );
};

export default MovieTicketBooking;
